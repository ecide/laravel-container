# Laravel container

Projeto para agilizar a montagem de containers contendo PHP7+, Composer, Laravel, MySQL e Nginx.

## Getting Started

### Instalando

Faça o clone do projeto normalmente:

```
git clone https://gitlab.com/ecide/laravel-container.git
```

Acesse a pasta do projeto e crie um novo projeto laravel com seu assistente:

```
laravel new blog
```

Configure um novo arquivo .env:

```
cp example.env .env
```

Altere o .env com o caminho da aplicação:

```
APP_PATH=./blog
```

As configurações do Nginx estarão em ./conf/app.conf, altere como desejar.

Altere as configurações de acesso ao seu banco no Laravel:

```
'host' => 'db'
```

Ou no .env:

```
DB_HOST=db
```


## Rodando

Dentro da pasta do projeto, rode:

```
docker-compose up -d
```

Acesse o server_name escolhido em app.conf e GG.
